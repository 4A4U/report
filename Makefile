.PHONY: clean cleandist

AT:=@
TEXFILES:=$(shell find -name '*.tex')
OUTPUT:= #>/dev/null 2>&1
PDFLATEX:=pdflatex -interaction=nonstopmode -file-line-error
LANGUAGES:=french english translation
ALL:=$(patsubst %,rapport_%.pdf,$(LANGUAGES))

define flatten
	$(AT)gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -DsAFER -sOutputFile="$2" "$1"
endef

all: $(ALL)

rapport_%.pdf: $(TEXFILES)
	$(AT)sed -i 's/\[.*\]{langues}/\[$*\]{langues}/' src/entete.lang
	$(AT)$(PDFLATEX) rapport.tex $(OUTPUT)
	$(AT)$(PDFLATEX) rapport.tex $(OUTPUT)
	$(AT)$(PDFLATEX) rapport.tex $(OUTPUT)
	$(AT)$(call flatten,rapport.pdf,$@)

clean:
	$(AT)rm -f rapport.toc *.log rapport.aux rapport.out rapport.bbl rapport.blg rapport.pdf

cleandist: clean
	$(AT)rm -f $(ALL)
